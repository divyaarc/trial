Trial::Application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"

   #root 'users#home'

root 'users#index'
 resources :users
 resources :sessions
 resources :films do#, only: [:index, :show, :book]
  resources :shows
end

 get 'users/new' => 'users#new'
 post 'users/new' => 'users#create'
 get 'sessions/login' => 'sessions#show'
 post 'sessions/login' => 'sessions#login_attempt'
 post 'sessions/logout' => 'users#index'
 
match "/films/show/:film_id" => "films#show", :via => [:get], :as => "show"
#get "films/show/:film_id/shows/new?id=:show_id" => 'shows#new'
get "films/:film_id/shows/new/:show_id" => 'shows#new'
post "films/:film_id/shows/new" => 'shows#create'
get "films/:film_id/shows/new" => 'shows#view'
#match "films/show/:film_id/shows/new/:show_id" => "shows#new", :via => [:get], :as => "new"
#match "/films/show/:film_id/:show_id" => "films#book", :via => [:get], :as => "book"
#match "/films/show/:film_id/book/:show_id" => "films#book", :via => [:get], :as =>"book"
#get "films/show/:film_id/book/:show_id/" => "films#book"

 #get ':controller(/:action(/:id))(.:format)'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
