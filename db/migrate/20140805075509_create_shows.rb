class CreateShows < ActiveRecord::Migration
  def change
    create_table :shows do |t|
      t.text :timing
      t.integer :vacancies

      t.timestamps
    end
  end
end
