class AddFilmRefToShow < ActiveRecord::Migration
  def change
    add_reference :shows, :film, index: true
  end
end
