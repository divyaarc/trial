class AddFilmRefToBooking < ActiveRecord::Migration
  def change
    add_reference :bookings, :film, index: true
  end
end
