class ShowsController < ApplicationController

def new
	    @book=Booking.new
		@film=Film.find(params[:film_id])
		@show=Show.find(params[:id])
end
def create
	@book=Booking.new(booking_params)
		if @book.save
			flash[:now]= "Created!"
			render 'view'
		else
			render 'new'
		end
end
def view
 @book=Booking.find(params[:booking_id])
 @film=Film.find(params[:film_id])
 @show=Show.find(params[:show_id])
end


private
	def booking_params
		params.require(:booking).permit(:seats, :user_id, :show_id)
	end

end
