class SessionsController < ApplicationController


def login
end

def show
	end

def login_attempt
		authorized_user = User.authenticate(params[:name],params[:login_password])
			if authorized_user
				session[:user_id] = authorized_user.id
				flash[:notice] = "Welcome, you are now logged in as #{authorized_user.name}"
				redirect_to films_path
			else
				flash[:notice] = "Invalid Username or Password"
				render "show"
			end
end


def logout
	session[:user_id] = nil
	redirect_to :root
end

def home
	@current_user=User.all
	
end

before_filter :authenticate_user, :only => [:home]
before_filter :save_login_state, :only => [:login, :login_attempt]

end
